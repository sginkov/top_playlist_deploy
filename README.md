# Installation steps to use chef scripts

#### 1. Install ruby

#### 2. Install chef solo with dependencies

```bash
gem install bundler && bundle install
```

#### 3. Install cookbook dependencies

```bash
librarian-chef install
```

# Deploy

All nodes are located into directory 'nodes'.

First of all you should install chef-client on taget server

```
knife solo prepare [user]@[node] --forward-agent 
```

You should execute the following command to run chef script for you node:

```bash
knife solo cook [user]@[node] --forward-agent 
```

Example for test.com:

```bash
knife solo cook root@test.com --forward-agent
knife solo prepare vagrant@[HOST] -i ~/.vagrant.d/insecure_private_key --forward-agent 
```

# Deploy notes

Cookbook 'mediatron' creates fakes config from template! Developer has to modify them after first deploying!!! 

# Testing chef scripts with vagrant

Vagrant helps to create virtual host on your local machine which used for emulating external virtual hosts.

## Installation vagrant steps 

#### 1. Install virtualbox and vagrant

#### 2. Install vagrant plugins

```
vagrant plugin install vagrant-hostsupdater
vagrant plugin install vagrant-omnibus
```

#### 3. Add dummy box

```
vagrant box add precise32 http://files.vagrantup.com/precise32.box
vagrant box add ubuntu-14.04 https://oss-binaries.phusionpassenger.com/vagrant/boxes/latest/ubuntu-14.04-amd64-vbox.box
```

#### 4. Start vagrant

This command create local virtual hosts which described into Vagrantfile.

```
vagrant up
```

#### 5. Download backups

```
scp -P 4567 deploy@188.166.12.67:/var/www/top_playlist/shared/upload-2015-02-01-1306.tar ../backups
```
