default[:frontend][:deploy_to] = '/var/www/frontend'
default[:frontend][:host] = 'localhost'

default[:frontend][:repo_host] = "bitbucket.org"
default[:frontend][:repository] = "git@bitbucket.org:sginkov/top_playlist_client.git"
default[:frontend][:revision] = "master"
default[:frontend][:keep_releases] = 5

default[:frontend][:user] = 'root'
default[:frontend][:group] = 'root'

default[:frontend][:env] = 'production'
default[:frontend][:config][:base_api_url]  = 'localhost'
default[:frontend][:config][:assets_url] = 'localhost'

default[:frontend][:package] = 'frontend'
default[:frontend][:prerender_token] = 'VTuf50lsHVqnEWwBuCYu'