include_recipe "nginx"
include_recipe "git"
include_recipe "nodejs"
include_recipe 'rvm::system'
include_recipe "grunt_cookbook::install_grunt_cli"

# canvas dependencies
apt_package "gifsicle libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++" do
  action :install
end

nginx_site 'default' do
  enable false
end

['compass'].each do |gem_name|
  rvm_global_gem gem_name do
    action :install
  end
end

ssh_known_hosts_entry(node[:frontend][:repo_host])

deploy_to = node[:frontend][:deploy_to]
host = node[:frontend][:host]

shared_dirs = ["node_modules", "cached-copy"]

shared_dirs.each do |dir|
  directory "#{deploy_to}/shared/#{dir}" do
    owner node[:frontend][:user]
    group node[:frontend][:group]
    recursive true
    action :create
  end
end

deploy(deploy_to) do
  scm_provider Chef::Provider::Git
  repo node[:frontend][:repository]
  revision node[:frontend][:revision]

  user node[:frontend][:user]
  group node[:frontend][:group]
  keep_releases node[:frontend][:keep_releases]

  action :deploy
  create_dirs_before_symlink.clear
  purge_before_symlink.clear
  symlink_before_migrate.clear
  symlinks.clear

  before_symlink do

    shared_dirs.each do |dir|
      directory "#{release_path}/#{dir}" do
        recursive true
        action :delete
      end

      link "#{release_path}/#{dir}" do
        to "#{deploy_to}/shared/#{dir}"
        link_type :symbolic
      end
    end

    nodejs_npm node[:frontend][:package] do
      json true
      path release_path
    end

    # bower
      nodejs_npm "bower" do
        options ['--silent', '--global']
      end

    #   execute "bower install --allow-root" do
    #     user node[:frontend][:user]
    #     group node[:frontend][:group]
    #     cwd release_path
    #   end

    # # grunt build
    #   directory "#{release_path}/dist" do
    #     recursive true
    #     action :delete
    #   end

    #   if node[:frontend][:config]
    #     directory "#{release_path}/config" do
    #       recursive true
    #       action :create
    #     end

    #     template "#{release_path}/config/#{node[:frontend][:env]}.json" do
    #       source "config.json.erb"
    #       mode 0777
    #       owner node[:frontend][:user]
    #       group node[:frontend][:group]
    #       variables(config: node[:frontend][:config])
    #     end

    #     ENV['ENV'] = node[:frontend][:env]
    #   end

    #   rvm_shell "grunt build" do
    #     cwd release_path
    #     user node[:frontend][:user]
    #     group node[:frontend][:group]

    #     code %{
    #       grunt build
    #     }
    #   end
  end

  notifies :restart, "service[nginx]"
end

nginx_site 'default' do
  enable false
end

template "/etc/nginx/sites-available/frontend" do
  source "vhost.conf.erb"
  mode 0777
  owner node[:nginx][:user]
  group node[:nginx][:group]
  variables(host: host, path: deploy_to, prerender_token: node[:frontend][:prerender_token])
  notifies :restart, 'service[nginx]'
end

template "/etc/nginx/mime.types" do
  source "mime.types"
  mode 0440
  owner node[:nginx][:user]
  group node[:nginx][:group]
  notifies :restart, 'service[nginx]'
end

nginx_site 'frontend' do
  enable true
end