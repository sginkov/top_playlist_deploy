default[:backend][:deploy_to] = '/var/www/top_playlist'

default[:backend][:repo_host] = 'bitbucket.org'
default[:backend][:repository] = 'git@bitbucket.org:sginkov/top_playlist.git'
default[:backend][:revision] = 'master'
default[:backend][:keep_releases] = 5

default[:backend][:unicorn][:dir] = '/etc/unicorn'
default[:backend][:unicorn][:sock] = '/tmp/backend.unicorn.socket'
default[:backend][:unicorn][:pid] = '/var/run/backend.unicorn.pid'
default[:backend][:unicorn][:stderr] = '/var/log/backend.unicorn.stderr.log'
default[:backend][:unicorn][:stdout] = '/var/log/backend.unicorn.stdout.log'
default[:backend][:unicorn][:worker_processes] = 4

default[:backend][:redis][:server] = 'localhost'
default[:backend][:redis][:port] = 6379
default[:backend][:redis][:db_num] = 0
default[:backend][:redis][:namespace] = 'top_playlist_production'

default[:backend][:mongoid][:database] = 'top_playlist_production'
default[:backend][:mongoid][:host] = 'localhost:27017'

default[:backend][:elasticsearch][:host] = 'http://localhost:9200/'

default[:backend][:user] = 'root'
default[:backend][:group] = 'root'

default[:backend][:env] = "production"
default[:backend][:seeds] = false

default[:backend][:secret_key_base] = nil

default[:backend][:nginx][:host] = 'localhost'
default[:backend][:nginx][:port] = '80'
default[:backend][:sidekiq][:concurrency] = 1

default[:backend][:hipchat][:api_token] = 'ec747970884c53881e28d32069d9f3'
default[:backend][:hipchat][:username] = 'production_backend'
default[:backend][:hipchat][:room] = 'top_playlist'