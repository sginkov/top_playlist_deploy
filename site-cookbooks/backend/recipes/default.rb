include_recipe "nginx"
include_recipe "runit"

include_recipe 'apt'
include_recipe 'rvm::system'
include_recipe 'git'

ENV['DEBIAN_FRONTEND'] = 'noninteractive'
package 'nginx-extras' do
  options '--force-yes'
  options '-o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"'
  action :install
end

['bundler', 'foreman'].each do |gem_name|
  rvm_global_gem gem_name do
    action :install
  end
end

directory node[:backend][:unicorn][:dir] do
  owner node[:backend][:user]
  group node[:backend][:group]
  mode 00755
end

conf_folder = File.join(node[:backend][:unicorn][:dir], 'backend')
directory conf_folder do
  owner node[:backend][:user]
  group node[:backend][:group]
  mode 00755
end

conf_file = File.join(conf_folder, "unicorn.conf.rb")
template conf_file do
  source 'unicorn.conf.rb.erb'
  owner node[:backend][:user]
  group node[:backend][:group]
  mode 0644
end

runit_service "backend_unicorn" do
  sv_timeout 30
  supports status: false, reload: false
end

deploy_to = node[:backend][:deploy_to]

runit_service "sidekiq" do
  sv_timeout 30
  supports status: false, reload: false
  options({ 
    env: node[:backend][:env],
    deploy_to: deploy_to,
    config_path: "#{deploy_to}/current/config/sidekiq.yml",
    log_path: "#{deploy_to}/shared/log/sidekiq.log",
    pid_path: "#{deploy_to}/shared/pids/sidekiq.pid"
  })
end

directory(File.join(deploy_to, 'shared')) do
  owner node[:backend][:user]
  group node[:backend][:group]
  mode 00777
  recursive true
end

template File.join(deploy_to, 'shared', 'backup_script.sh') do
  source 'backup_script.sh.erb'
  owner node[:backend][:user]
  group node[:backend][:group]
  mode 0644
end

['log', 'pids', 'system', 'config', 'uploads'].each do |dir_name|
  directory(File.join(deploy_to, 'shared', dir_name)) do
	  owner node[:backend][:user]
	  group node[:backend][:group]
    mode 00775
  end
end

config_files = [
  'redis.yml',
  'elasticsearch.yml',
  'mongoid.yml',
  'sidekiq.yml',
  'hipchat.yml',
  'newrelic.yml',
  'settings.yml'
]

config_files.each do |config_file|
  template File.join(deploy_to, 'shared', 'config', config_file) do
    source "#{config_file}.erb"
		owner node[:backend][:user]
		group node[:backend][:group]
    mode 0644
  end
end

ssh_known_hosts_entry(node[:backend][:repo_host])

# package for minimagick
package 'imagemagick'

deploy(deploy_to) do
  scm_provider Chef::Provider::Git
  environment "RAILS_ENV" => node[:backend][:env]
  repo node[:backend][:repository]
  revision node[:backend][:revision]

  user node[:backend][:user]
  group node[:backend][:group]
  keep_releases node[:backend][:keep_releases]

  action :deploy

  symlinks_hash = config_files.inject({'uploads' => 'public/uploads'}) do |memo, config_file|
    config_file_path = "config/#{config_file}"
    memo[config_file_path] = config_file_path
    memo
  end

  symlinks(symlinks_hash)

  before_restart do
    rvm_shell "Bundle install" do
      cwd release_path
  		user node[:backend][:user]
  		group node[:backend][:group]

      code %{
        bundle install --path #{deploy_to}/shared/bundle --deployment --without development test
      }
    end


    if node[:backend][:seeds]
      rvm_shell "Seeds" do
        cwd release_path
        user node[:backend][:user]
        group node[:backend][:group]

        code %{
          export RAILS_ENV=#{node[:backend][:env]}
          bundle exec rake db:seed
        }
      end
    end  

    rvm_shell "Assets precompile" do
      cwd release_path
      user node[:backend][:user]
      group node[:backend][:group]

      code %{
        export RAILS_ENV=#{node[:backend][:env]}
        bundle exec rake assets:precompile
      }
    end

    rvm_shell "Whenever" do
      cwd release_path
  		user node[:backend][:user]
  		group node[:backend][:group]

      code %{
        export RAILS_ENV=#{node[:backend][:env]}
        bundle exec whenever -w
      }
    end
  end

  notifies :restart, "runit_service[backend_unicorn]"
  notifies :restart, "runit_service[sidekiq]"
end

nginx_site 'default' do
  enable false
end

template "/etc/nginx/sites-available/backend" do
  source "vhost.conf.erb"
  mode 0777
  owner node[:nginx][:user]
  group node[:nginx][:user]
  notifies :restart, 'service[nginx]'
end

nginx_site "backend" do
  enable true
end